﻿using Google.Protobuf;

using Moq;

using Rpc.Api;

using server.Models;
using server.Rpcs;
using server.Services;
using server.Tests.Utils;

using System;
using System.Threading.Tasks;

using Xunit;

namespace server.Tests.Rpcs
{
    public class ApiServiceImplTests
    {
        [Fact]
        public async Task Write_呼び出されたらEndPointを呼ぶ()
        {
            // serviceEndPointを用意
            var serviceEndPoint = new Mock<IServiceEndPoint>();

            // Writeの引数を用意
            var request = new WriteRequest()
            {
                Name = "test1.cs",
                Content = ByteString.CopyFromUtf8("public class test1 {}")
            };
            var context = ServerCallContextUtil.Mock;

            // ApiServiceImplをインスタンス化
            var apiServiceImpl = new ApiServiceImpl(serviceEndPoint.Object);

            // Writeを実行
            var ret = await apiServiceImpl.Write(request, context);

            // 実行結果を検証
            serviceEndPoint.Verify(s => s.WriteAsync(It.IsAny<File>()));
            Assert.Equal(WriteResponse.Types.ResStatus.Ok, ret.Status);
        }

        [Fact]
        public async Task Write_例外発生時にエラーを返す()
        {
            // serviceEndPointを用意
            var serviceEndPoint = new Mock<IServiceEndPoint>();
            serviceEndPoint
                .Setup(s => s.WriteAsync(It.IsAny<File>()))
                .Throws(new NotImplementedException());

            // Writeの引数を用意
            var request = new WriteRequest()
            {
                Name = "test1.cs",
                Content = ByteString.CopyFromUtf8("public class test1 {}")
            };
            var context = ServerCallContextUtil.Mock;

            // ApiServiceImplをインスタンス化
            var apiServiceImpl = new ApiServiceImpl(serviceEndPoint.Object);

            // Writeを実行
            var ret = await apiServiceImpl.Write(request, context);

            // 実行結果を検証
            serviceEndPoint.Verify(s => s.WriteAsync(It.IsAny<File>()));
            Assert.Equal(WriteResponse.Types.ResStatus.Error, ret.Status);
        }
    }
}