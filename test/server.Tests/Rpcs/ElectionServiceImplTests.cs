﻿using Grpc.Core;

using Moq;

using Rpc.Election;

using server.Models;
using server.Rpcs;
using server.Services;
using server.Tests.Utils;

using System;
using System.Threading.Tasks;

using Xunit;

namespace server.Tests.Rpcs
{
    public class ElectionServiceImplTests
    {
        [Fact]
        public async Task Election_選任を引き継ぐ()
        {
            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .Setup(o => o.Election());

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(o => o.MyNode)
                .Returns(new Node() { Id = 2 });

            var req = new ElectionRequest()
            {
                Id = 3
            };
            var context = ServerCallContextUtil.Mock;

            var electionServiceImpl = new ElectionServiceImpl(overlayNetwork.Object, nodeContext.Object);

            // doing!
            var res = await electionServiceImpl.Election(req, context);

            Assert.Equal(ElectionResponse.Types.ResStatus.Ok, res.Status);
            overlayNetwork.Verify(o => o.Election(), Times.Once);
            overlayNetwork.Verify(o => o.SetMasterAsync(It.IsAny<Node>()), Times.Never);
            overlayNetwork.VerifyNoOtherCalls();
            nodeContext.VerifyGet(o => o.MyNode);
            nodeContext.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task Election_Errorを通知()
        {
            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .Setup(o => o.Election());

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(o => o.MyNode)
                .Returns(new Node() { Id = 3 }); // reqと同値

            var req = new ElectionRequest()
            {
                Id = 3
            };
            var context = ServerCallContextUtil.Mock;

            var electionServiceImpl = new ElectionServiceImpl(overlayNetwork.Object, nodeContext.Object);

            // doing!
            var res = await electionServiceImpl.Election(req, context);

            Assert.Equal(ElectionResponse.Types.ResStatus.Error, res.Status);
            overlayNetwork.Verify(o => o.Election(), Times.Never);
            overlayNetwork.VerifyNoOtherCalls();
            nodeContext.VerifyGet(o => o.MyNode);
            nodeContext.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task Election_例外を処理()
        {
            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .Setup(o => o.Election())
                .Throws(new NotSupportedException());

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(o => o.MyNode)
                .Returns(new Node() { Id = 1 });

            var req = new ElectionRequest()
            {
                Id = 4
            };
            var context = ServerCallContextUtil.Mock;

            var electionServiceImpl = new ElectionServiceImpl(overlayNetwork.Object, nodeContext.Object);

            // doing!
            var res = await electionServiceImpl.Election(req, context);

            Assert.Equal(ElectionResponse.Types.ResStatus.Error, res.Status);
            Assert.Equal(StatusCode.Internal, context.Status.StatusCode);
            overlayNetwork.Verify(o => o.Election(), Times.Once);
            overlayNetwork.VerifyNoOtherCalls();
            nodeContext.VerifyGet(o => o.MyNode);
            nodeContext.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task Coordinator_MasterNodeを更新()
        {
            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .Setup(o => o.SetMasterAsync(It.IsAny<Node>()))
                .Callback<Node>(x =>
                {
                    Assert.Equal(2, x.Id);
                    Assert.Equal("prac2.ds.soft.iwate-pu.ac.jp", x.Host);
                    Assert.Equal(50026, x.Port);
                })
                .Returns(Task.CompletedTask);

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(x => x.MyNode)
                .Returns(new Node()
                {
                    Id = 3
                });

            var req = new CoordinatorRequest()
            {
                Id = 2,
                Host = "prac2.ds.soft.iwate-pu.ac.jp",
                Port = 50026
            };
            var context = ServerCallContextUtil.Mock;

            var electionServiceImpl = new ElectionServiceImpl(overlayNetwork.Object, nodeContext.Object);

            // doing!
            var res = await electionServiceImpl.Coordinator(req, context);

            overlayNetwork.Verify(o => o.SetMasterAsync(It.IsAny<Node>()), Times.Once);
            overlayNetwork.VerifyNoOtherCalls();
            Assert.Equal(CoordinatorResponse.Types.ResStatus.Ok, res.Status);
        }

        [Fact]
        public async Task Coordinator_例外を処理()
        {
            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .Setup(o => o.SetMasterAsync(It.IsAny<Node>()))
                .Throws(new NotSupportedException());

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(x => x.MyNode)
                .Returns(new Node()
                {
                    Id = 3
                });

            var req = new CoordinatorRequest()
            {
                Id = 1,
                Host = "prac1.ds.soft.iwate-pu.ac.jp",
                Port = 50025
            };
            var context = ServerCallContextUtil.Mock;

            var electionServiceImpl = new ElectionServiceImpl(overlayNetwork.Object, nodeContext.Object);

            // doing!
            var res = await electionServiceImpl.Coordinator(req, context);

            Assert.Equal(CoordinatorResponse.Types.ResStatus.Error, res.Status);
            Assert.Equal(StatusCode.Internal, context.Status.StatusCode);
            overlayNetwork.Verify(o => o.SetMasterAsync(It.IsAny<Node>()), Times.Once);
            overlayNetwork.VerifyNoOtherCalls();
        }
    }
}