﻿using Grpc.Core;
using Grpc.Core.Testing;

using System;
using System.Threading;

namespace server.Tests.Utils
{
    public static class ServerCallContextUtil
    {
        public static ServerCallContext Mock
            => TestServerCallContext.Create(
                    null, // method
                    null, // host
                    DateTime.MaxValue, // deadline
                    null, // requestHeaders
                    CancellationToken.None,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);
    }
}