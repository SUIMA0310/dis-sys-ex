﻿using server.Models;

namespace server.Tests.Utils
{
    public static class FileUtil
    {
        public static File GetFile()
            => new File
            {
                FileId = 1,
                Name = "test",
                Content = new byte[5],
                InsertTime = System.DateTime.Now
            };
    }
}