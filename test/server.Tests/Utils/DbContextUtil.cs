﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

using System.Data.Common;

namespace server.Tests.Utils
{
    public static class DbContextUtil
    {
        public static readonly DbConnection Connection = new SqliteConnection("DataSource=:memory:");

        public static DbContextOptions<T> GetOptions<T>(DbConnection connection) where T : DbContext
            => new DbContextOptionsBuilder<T>()
                        .UseSqlite(connection)
                        .Options;
    }
}