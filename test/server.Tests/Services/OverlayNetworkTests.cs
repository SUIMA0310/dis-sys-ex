﻿using Microsoft.EntityFrameworkCore;

using Moq;

using server.Models;
using server.Services;
using server.Rpcs.Clients;

using System.Threading;
using System.Threading.Tasks;

using Rpc.Election;

using Xunit;
using static Rpc.Election.ElectionService;
using System;
using Rpc.Election.Utils;
using Grpc.Core;
using Grpc.Core.Testing;

namespace server.Tests.Services
{
    public class OverlayNetworkTests
    {
        [Fact]
        public void 他ノードが死んでいて自身がMasterになる()
        {
            var option = new Mock<Options>();
            option.SetupGet(o => o.InstanceId).Returns(2);

            // 2つのノードが存在し、他ノードは不活性状態
            var otherNode = new Mock<Node>();
            otherNode.SetupProperty(n => n.Id, 1);
            otherNode.SetupProperty(n => n.Host, "prac1.ds.soft.iwate-pu.ac.jp");
            otherNode.SetupProperty(n => n.Port, 50025);
            otherNode.Setup(n => n.CreateChannel());

            var myNode = new Mock<Node>();
            myNode.SetupProperty(n => n.Id, 2);
            myNode.SetupProperty(n => n.Host, "prac2.ds.soft.iwate-pu.ac.jp");
            myNode.SetupProperty(n => n.Port, 50025);

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(c => c.AllNodes)
                .Returns(new[] { otherNode.Object, myNode.Object });
            nodeContext
                .SetupGet(c => c.MyNode)
                .Returns(myNode.Object);
            nodeContext
                .SetupGet(c => c.OtherNodes)
                .Returns(new[] { otherNode.Object });
            nodeContext
                .Setup(c => c.GetNodeFromId(1))
                .Returns(otherNode.Object);
            nodeContext
                .Setup(c => c.GetNodeFromId(2))
                .Returns(myNode.Object);

            var client = new Mock<ElectionServiceClient>();
            // 次ノードへの選任継続依頼に失敗
            client.Setup(c => c.ElectionAsync(
                It.IsAny<ElectionRequest>(),
                null,
                It.IsAny<DateTime>(),
                CancellationToken.None))
                .Throws(new RpcException(new Status()))
                .Verifiable();
            client.Setup(c => c.CoordinatorAsync(
                It.IsAny<CoordinatorRequest>(),
                null,
                It.IsAny<DateTime>(),
                CancellationToken.None))
                .Throws(new RpcException(new Status()))
                .Verifiable();

            var clientFactory = new Mock<IClientFactory<ElectionServiceClient>>();
            clientFactory
                .Setup(f => f.GetClient(null))
                .Returns(client.Object);

            var tokenSource = new CancellationTokenSource();

            var overlayNetwork = new OverlayNetwork(
                option.Object,
                nodeContext.Object,
                clientFactory.Object,
                tokenSource.Token);

            overlayNetwork.Election();

            tokenSource.Cancel();

            overlayNetwork.Dispose();
            tokenSource.Dispose();

            otherNode.Verify(n => n.CreateChannel(), Times.Once);
            //clientFactory.Verify(f => f.GetClient(null));

            Assert.True(overlayNetwork.IsMaster);
            Assert.Null(overlayNetwork.MasterChannel);
        }


        [Fact]
        public void 自身がMasterになるべきノード()
        {
            var option = new Mock<Options>();
            option.SetupGet(o => o.InstanceId).Returns(2);

            // 2つのノードが存在し、他ノードは活性状態
            var myNode = new Mock<Node>();
            myNode.SetupProperty(n => n.Id, 1);
            myNode.SetupProperty(n => n.Host, "prac1.ds.soft.iwate-pu.ac.jp");
            myNode.SetupProperty(n => n.Port, 50025);

            var otherNode = new Mock<Node>();
            otherNode.SetupProperty(n => n.Id, 2);
            otherNode.SetupProperty(n => n.Host, "prac2.ds.soft.iwate-pu.ac.jp");
            otherNode.SetupProperty(n => n.Port, 50025);
            otherNode.Setup(n => n.CreateChannel());

            var nodeContext = new Mock<INodeContext>();
            nodeContext
                .SetupGet(c => c.AllNodes)
                .Returns(new[] { otherNode.Object, myNode.Object });
            nodeContext
                .SetupGet(c => c.MyNode)
                .Returns(myNode.Object);
            nodeContext
                .SetupGet(c => c.OtherNodes)
                .Returns(new[] { otherNode.Object });
            nodeContext
                .Setup(c => c.GetNodeFromId(1))
                .Returns(myNode.Object);
            nodeContext
                .Setup(c => c.GetNodeFromId(2))
                .Returns(otherNode.Object);

            var clientRet = TestCalls.AsyncUnaryCall(
                Task.FromResult(CoordinatorResponseUtil.Ok),
                Task.FromResult(new Metadata()),
                () => Status.DefaultSuccess,
                () => new Metadata(),
                () => { });

            var client = new Mock<ElectionServiceClient>();
            // 次ノードへがいない
            client.Setup(c => c.ElectionAsync(
                It.IsAny<ElectionRequest>(),
                null,
                It.IsAny<DateTime>(),
                CancellationToken.None))
                .Throws(new RpcException(new Status()))
                .Verifiable();
            client.Setup(c => c.CoordinatorAsync(
                It.IsAny<CoordinatorRequest>(),
                null,
                It.IsAny<DateTime>(),
                CancellationToken.None))
                .Returns(clientRet)
                .Verifiable();

            var clientFactory = new Mock<IClientFactory<ElectionServiceClient>>();
            clientFactory
                .Setup(f => f.GetClient(null))
                .Returns(client.Object);

            var tokenSource = new CancellationTokenSource();

            var overlayNetwork = new OverlayNetwork(
                option.Object,
                nodeContext.Object,
                clientFactory.Object,
                tokenSource.Token);

            overlayNetwork.Election();

            tokenSource.Cancel();

            overlayNetwork.Dispose();
            tokenSource.Dispose();

            otherNode.Verify(n => n.CreateChannel(), Times.Once);
            //clientFactory.Verify(f => f.GetClient(null));

            Assert.True(overlayNetwork.IsMaster);
            Assert.Null(overlayNetwork.MasterChannel);
        }
    }
}
