using Grpc.Core;
using Grpc.Core.Testing;

using Moq;

using Rpc.Api;

using server.Models;
using server.Rpcs.Clients;
using server.Services;

using System.Threading;
using System.Threading.Tasks;

using Xunit;

using static Rpc.Api.ApiService;

namespace server.Tests.Services
{
    public class ServiceEndPointTests
    {
        [Fact]
        public async Task Write_Masterに転送される()
        {
            var file = new File()
            {
                Name = "test.cs",
                Content = new byte[] { },
                InsertTime = System.DateTime.Now
            };

            var clientRet = TestCalls.AsyncUnaryCall<WriteResponse>(
                Task.FromResult(new WriteResponse() { Status = WriteResponse.Types.ResStatus.Ok }),
                Task.FromResult(new Metadata()),
                () => Status.DefaultSuccess,
                () => new Metadata(),
                () => { });

            var client = new Mock<ApiServiceClient>();
            client
                .Setup(c => c.WriteAsync(It.IsAny<WriteRequest>(), null, null, default(CancellationToken)))
                .Returns(clientRet);

            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .SetupGet(c => c.IsMaster)
                .Returns(false);

            var dataStore = new Mock<IDataStore>();
            var clientFactory = new Mock<IClientFactory<ApiServiceClient>>();
            clientFactory
                .Setup(c => c.GetClient(null))
                .Returns(client.Object);

            var serviceEndPoint = new ServiceEndPoint(
                dataStore.Object,
                overlayNetwork.Object,
                clientFactory.Object);

            // doing!
            await serviceEndPoint.WriteAsync(file);

            client.Verify(c => c.WriteAsync(It.IsAny<WriteRequest>(), null, null, default(CancellationToken)));
            client.VerifyNoOtherCalls();
            overlayNetwork.VerifyGet(c => c.IsMaster);
            overlayNetwork.VerifyGet(c => c.MasterChannel);
            overlayNetwork.VerifyNoOtherCalls();
            dataStore.VerifyNoOtherCalls();
            clientFactory.Verify(c => c.GetClient(null));
            clientFactory.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task Write_Masterに転送失敗()
        {
            var file = new File()
            {
                Name = "test.cs",
                Content = new byte[] { },
                InsertTime = System.DateTime.Now
            };

            var clientRet = TestCalls.AsyncUnaryCall<WriteResponse>(
                Task.FromResult(new WriteResponse() { Status = WriteResponse.Types.ResStatus.Error }),
                Task.FromResult(new Metadata()),
                () => Status.DefaultSuccess,
                () => new Metadata(),
                () => { });

            var client = new Mock<ApiServiceClient>();
            client
                .Setup(c => c.WriteAsync(It.IsAny<WriteRequest>(), null, null, default(CancellationToken)))
                .Returns(clientRet);

            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .SetupGet(c => c.IsMaster)
                .Returns(false);

            var dataStore = new Mock<IDataStore>();
            var clientFactory = new Mock<IClientFactory<ApiServiceClient>>();
            clientFactory
                .Setup(c => c.GetClient(null))
                .Returns(client.Object);

            var serviceEndPoint = new ServiceEndPoint(
                dataStore.Object,
                overlayNetwork.Object,
                clientFactory.Object);

            // doing!
            var ex = await Assert.ThrowsAsync<System.Exception>(async () =>
            {
                await serviceEndPoint.WriteAsync(file);
            });
            Assert.Equal("Transfer failure.", ex.Message);

            client.Verify(c => c.WriteAsync(It.IsAny<WriteRequest>(), null, null, default(CancellationToken)));
            client.VerifyNoOtherCalls();
            overlayNetwork.VerifyGet(c => c.IsMaster);
            overlayNetwork.VerifyGet(c => c.MasterChannel);
            overlayNetwork.VerifyNoOtherCalls();
            dataStore.VerifyNoOtherCalls();
            clientFactory.Verify(c => c.GetClient(null));
            clientFactory.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task Write_DataStorに書き込まれる()
        {
            var file = new File()
            {
                Name = "test.cs",
                Content = new byte[] { },
                InsertTime = System.DateTime.Now
            };

            var overlayNetwork = new Mock<IOverlayNetwork>();
            overlayNetwork
                .SetupGet(c => c.IsMaster)
                .Returns(true);

            var dataStore = new Mock<IDataStore>();
            dataStore
                .Setup(c => c.WriteAsync(It.Is<File>(f => f == file)))
                .Returns(Task.Delay(10));

            var clientFactory = new Mock<IClientFactory<ApiServiceClient>>();

            var serviceEndPoint = new ServiceEndPoint(
                dataStore.Object,
                overlayNetwork.Object,
                clientFactory.Object);

            // doing!
            await serviceEndPoint.WriteAsync(file);

            overlayNetwork.VerifyGet(c => c.IsMaster);
            overlayNetwork.VerifyNoOtherCalls();
            dataStore.Verify(c => c.WriteAsync(It.Is<File>(f => f == file)));
            dataStore.VerifyNoOtherCalls();
            clientFactory.VerifyNoOtherCalls();
        }
    }
}