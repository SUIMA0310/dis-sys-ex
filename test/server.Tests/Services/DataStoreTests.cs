﻿using Microsoft.EntityFrameworkCore;

using Moq;

using server.Models;
using server.Rpcs.Clients;
using server.Services;

using System.Threading;
using System.Threading.Tasks;

using Xunit;
using static Rpc.Sync.SyncService;
using static server.Tests.Utils.DbContextUtil;
using static server.Tests.Utils.FileUtil;

namespace server.Tests.Service
{
    public class DataStoreTests
    {
        [Fact]
        public async Task Write_FileをWriteしDBに書き込まれる()
        {
            using (var connection = Connection)
            {
                connection.Open();
                var dbOptions = GetOptions<FileContext>(connection);
                using (var fileContext = new FileContext(dbOptions))
                using (var cancel = new CancellationTokenSource())
                {
                    var overlayNetwork = new Mock<IOverlayNetwork>();
                    overlayNetwork.SetupGet(o => o.IsMaster).Returns(true);

                    var clientFactory = new Mock<IClientFactory<SyncServiceClient>>();

                    var dataStore = new DataStore(fileContext, overlayNetwork.Object, clientFactory.Object, cancel.Token);

                    var file = GetFile();
                    await dataStore.WriteAsync(file);

                    Assert.Equal(1, fileContext.Files.CountAsync().Result);
                }
            }
        }

        [Fact]
        public async Task OnWrite_Writeを実行すると登録したActionが呼ばれる()
        {
            using (var connection = Connection)
            {
                connection.Open();
                var dbOptions = GetOptions<FileContext>(connection);
                using (var fileContext = new FileContext(dbOptions))
                using (var cancel = new CancellationTokenSource())
                {
                    var overlayNetwork = new Mock<IOverlayNetwork>();
                    overlayNetwork.SetupGet(o => o.IsMaster).Returns(true);

                    var clientFactory = new Mock<IClientFactory<SyncServiceClient>>();

                    var dataStore = new DataStore(fileContext, overlayNetwork.Object, clientFactory.Object, cancel.Token);

                    bool isCalled = false;

                    dataStore.OnWriteSync(res => { isCalled = true; });
                    await dataStore.WriteAsync(GetFile());

                    Assert.True(isCalled);
                }
            }
        }
    }
}