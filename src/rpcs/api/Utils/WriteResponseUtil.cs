﻿namespace Rpc.Api.Utils
{
    public static class WriteResponseUtil
    {
        private static WriteResponse _Ok = null;
        private static WriteResponse _Error = null;

        public static WriteResponse Ok
            => _Ok ?? (_Ok = new WriteResponse() { Status = WriteResponse.Types.ResStatus.Ok });

        public static WriteResponse Error
            => _Error ?? (_Error = new WriteResponse() { Status = WriteResponse.Types.ResStatus.Error });
    }
}