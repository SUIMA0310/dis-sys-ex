﻿namespace Rpc.Api.Utils
{
    public static class ReadResponseUtil
    {
        private static ReadResponse _Error = null;
        private static ReadResponse _NotFound = null;

        public static ReadResponse Error
            => _Error ?? (_Error = new ReadResponse() { Status = ReadResponse.Types.ResStatus.Error });

        public static ReadResponse NotFound
            => _NotFound ?? (_NotFound = new ReadResponse() { Status = ReadResponse.Types.ResStatus.NotFound });
    }
}