namespace Rpc.Api.Utils
{
    public static class ReadRequestUtil
    {
        public static ReadRequest FromPath(string filePath)
        {
            return new ReadRequest()
            {
                Name = System.IO.Path.GetFileName(filePath)
            };
        }
    }
}