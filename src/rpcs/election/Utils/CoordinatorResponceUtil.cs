namespace Rpc.Election.Utils
{
    public static class CoordinatorResponseUtil
    {
        private static CoordinatorResponse _Ok = null;
        private static CoordinatorResponse _Error = null;

        public static CoordinatorResponse Ok
            => _Ok ?? (_Ok = new CoordinatorResponse() { Status = CoordinatorResponse.Types.ResStatus.Ok });

        public static CoordinatorResponse Error
            => _Error ?? (_Error = new CoordinatorResponse() { Status = CoordinatorResponse.Types.ResStatus.Error });
    }
}