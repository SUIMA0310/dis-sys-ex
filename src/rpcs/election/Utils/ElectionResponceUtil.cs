namespace Rpc.Election.Utils
{
    public static class ElectionResponseUtil
    {
        private static ElectionResponse _Ok = null;
        private static ElectionResponse _Error = null;

        public static ElectionResponse Ok
            => _Ok ?? (_Ok = new ElectionResponse() { Status = ElectionResponse.Types.ResStatus.Ok });

        public static ElectionResponse Error
            => _Error ?? (_Error = new ElectionResponse() { Status = ElectionResponse.Types.ResStatus.Error });
    }
}