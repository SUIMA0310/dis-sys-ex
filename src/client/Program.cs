﻿using client.Commands;

using Grpc.Core;

using McMaster.Extensions.CommandLineUtils;

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using static Rpc.Api.ApiService;

namespace client
{
    internal class Program
    {
        private const int DefaultPort = 50025;

        private static int Main(string[] args)
        {
            var app = new CommandLineApplication();
            app.Name = Assembly.GetExecutingAssembly().GetName().Name;
            app.Description = "分散システム実践論 演習課題 クライアントプログラム";
            app.HelpOption("-h|--help", inherited: true);
            var host = app.Argument("host", "Specify a remote server.").IsRequired();
            var port = app.Option<int>("-p|--port <port-no>", "Specify connection port.", CommandOptionType.SingleValue);
            var timeOut = app.Option<double>("-t|-timeOut <second>", "Connection timeout.", CommandOptionType.SingleValue);

            // Write
            var write = app.Command("write", writeCommand =>
            {
                writeCommand.Description = "Write to remote server.";
                var filePath = writeCommand.Argument("filePath", "Files to be sent.").IsRequired();
                writeCommand.OnExecute(() => CallCommandAsync(host, port, timeOut, filePath, WriteCommand.WriteAsync));
            });

            // Read
            var read = app.Command("read", readCommand =>
            {
                readCommand.Description = "Read from remote server.";
                var filePath = readCommand.Argument("filePath", "Files to be read.").IsRequired();
                readCommand.OnExecute(() => CallCommandAsync(host, port, timeOut, filePath, ReadCommand.ReadAsync));
            });

            app.OnExecute(() =>
            {
                // 引数が無ければヘルプを表示
                app.ShowHelp();
                return 0;
            });

            try
            {
                return app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine(ex.Message);
                app.ShowHelp();
                return -1;
            }
        }

        /// <summary>
        /// コマンドライン引数を基に、channelを作成する
        /// </summary>
        /// <param name="c_host"></param>
        /// <param name="c_port"></param>
        /// <returns></returns>
        private static Channel GetChannel(CommandArgument c_host, CommandOption<int> c_port)
        {
            var host = c_host.Value;
            var port = c_port.HasValue() ? c_port.ParsedValue : DefaultPort;

            return new Channel(host, port, ChannelCredentials.Insecure);
        }

        /// <summary>
        /// channelからApiServiceClientを作成する
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        private static ApiServiceClient GetClient(Channel channel)
        {
            return new ApiServiceClient(channel);
        }

        /// <summary>
        /// タイムアウト秒数を取得する
        /// </summary>
        /// <param name="timeOut"></param>
        /// <param name="defaultValue"></param>
        /// <returns>タイムアウト秒数</returns>
        private static double GetTimeOut(CommandOption<double> timeOut, double defaultValue = 3.0)
        {
            return timeOut.HasValue() ? timeOut.ParsedValue : defaultValue;
        }

        /// <summary>
        /// デッドライン時刻を取得する
        /// </summary>
        /// <param name="timeOut">タイムアウト秒数</param>
        /// <returns>デッドライン時刻</returns>
        private static DateTime GetDeadLine(double timeOut)
        {
            return DateTime.UtcNow.AddSeconds(timeOut);
        }

        /// <summary>
        /// Commandの処理を呼び出す
        /// </summary>
        /// <param name="host">host name</param>
        /// <param name="port">connect port</param>
        /// <param name="arg">command argment</param>
        /// <param name="func">command</param>
        /// <param name="timeOut">connect time out (second)</param>
        /// <returns></returns>
        private static async Task CallCommandAsync(
            CommandArgument host,
            CommandOption<int> port,
            CommandOption<double> timeOut,
            CommandArgument arg,
            Func<ApiServiceClient, IEnumerable<string>, Task> func)
        {
            try
            {
                var channel = GetChannel(host, port);
                var client = GetClient(channel);
                var deadline = GetDeadLine(GetTimeOut(timeOut));

                await channel.ConnectAsync(deadline).ConfigureAwait(false);
                await func(client, arg.Values).ConfigureAwait(false);
                await channel.ShutdownAsync().ConfigureAwait(false);

                Console.WriteLine("----- SUCCESSFUL. -----");
            }
            catch (TaskCanceledException)
            {
                Console.Error.WriteLine("Can not connect to server.");
            }
            catch (Exception)
            {
                Console.Error.WriteLine("----- FAILED. -----");
            }
        }
    }
}