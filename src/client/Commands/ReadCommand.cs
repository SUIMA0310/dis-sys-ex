using Rpc.Api;
using Rpc.Api.Utils;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using static Rpc.Api.ApiService;

namespace client.Commands
{
    public class ReadCommand
    {
        public static async Task ReadAsync(ApiServiceClient client, IEnumerable<string> filePaths)
        {
            foreach (var filePath in filePaths)
            {
                // Read してくる
                var res = await client.ReadAsync(ReadRequestUtil.FromPath(filePath));

                if (res.Status == ReadResponse.Types.ResStatus.NotFound)
                {
                    Console.Error.WriteLine($"[Not Found] : {Path.GetFileName(filePath)}");
                    continue;
                }
                if (res.Status == ReadResponse.Types.ResStatus.Error)
                {
                    throw new Exception();
                }

                // 成功していたら、ファイルを書き込む
                if (res.Status == ReadResponse.Types.ResStatus.Ok)
                {
                    using (var fs = new FileStream(filePath, FileMode.OpenOrCreate))
                    {
                        await fs.WriteAsync(res.Content.ToByteArray());
                    }
                    Console.WriteLine("[Complite] : " + Path.GetFileName(filePath));
                }
            }
        }
    }
}