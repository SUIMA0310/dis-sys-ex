using Rpc.Api;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using static Rpc.Api.ApiService;

namespace client.Commands
{
    public class WriteCommand
    {
        public static async Task WriteAsync(ApiServiceClient client, IEnumerable<string> filePaths)
        {
            foreach (var filePath in filePaths)
            {
                using (var fs = new FileStream(filePath, FileMode.Open))
                {
                    // ファイルを終端まで読み、ファイル名を添えてwriteする
                    var res = await client.WriteAsync(new WriteRequest
                    {
                        Name = Path.GetFileName(filePath),
                        Content = await Google.Protobuf.ByteString.FromStreamAsync(fs).ConfigureAwait(false)
                    });

                    if (res.Status != WriteResponse.Types.ResStatus.Ok)
                    {
                        throw new Exception();
                    }
                }
                Console.WriteLine("[Complite] : " + Path.GetFileName(filePath));
            }
        }
    }
}