﻿namespace server
{
    public static class Constants
    {
        public static class CommandLineApplication
        {
            public const string Description = "分散システム実践論 演習課題 サーバプログラム";

            public static class Descriptions
            {
                public const string Port = "Specify connection port.";
                public const string DataFile = "Using DB path.";
            }
        }

        public static class Grpc
        {
            public const int DefaultPort = 50025;
            public const string DefaultHost = "localhost";
        }

        public static class FileContext
        {
            public const string DefaultDataFile = "./server.db";
        }
    }
}