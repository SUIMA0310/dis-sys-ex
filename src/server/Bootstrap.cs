﻿using DryIoc;

using Grpc.Core;

using McMaster.Extensions.CommandLineUtils;
using McMaster.Extensions.CommandLineUtils.Abstractions;

using NLog;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace server
{
    public class Bootstrap<TModel> where TModel : class
    {
        public virtual int Run(params string[] args)
        {
            var exitEvent = new ManualResetEvent(false);
            try
            {
                Log.Info("Initializing...");

                Log.Trace("CreateContainer");
                using (var container = CreateContainer())
                using (var tokenSource = new CancellationTokenSource())
                {
                    Log.Debug("Init cancel handller");
                    Console.CancelKeyPress += (s, e) =>
                    {
                        Log.Trace("Cancel Key Pressd.");
                        tokenSource.Cancel();

                        Log.Trace("Wait for exitEvent.");
                        exitEvent.WaitOne();

                        Log.Info("Good bye.");
                    };

                    Log.Trace("CreateCommandLineApplication");
                    var app = CreateCommandLineApplication();

                    Log.Trace("ConfigureCommandLineApplication");
                    ConfigureCommandLineApplication(app);
                    app.OnExecute(() => Execute(container));

                    Log.Trace("ConfigureContainer");
                    ConfigureContainer(container);

                    Log.Trace("ContainerRegister Model");
                    container.RegisterDelegate(c => GetModel(app));

                    Log.Trace("ContainerRegister CancellationToken");
                    container.RegisterDelegate(c => tokenSource.Token, Reuse.Singleton);

                    Log.Debug("OnInitActions");
                    OnInitActions(container);

                    Log.Debug("Execute");
                    return app.Execute(args);
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
                return -1;
            }
            finally
            {
                Log.Info("Exited");
                exitEvent.Set();
            }
        }

        protected virtual async Task<int> Execute(Container container)
        {
            var token = container.Resolve<CancellationToken>();
            var server = container.Resolve<Server>();

            Log.Info("Starting server.");
            server.Start();

            Log.Info("Press CTRL + C to exit.");
            // メインスレッド止めると死ぬ
            await Task.Run(() => token.WaitHandle.WaitOne());

            Log.Info("Stopping...");
            await server.ShutdownAsync();

            Log.Info("Stoped");
            return 0;
        }

        protected virtual Container CreateContainer()
        {
            return new Container();
        }

        protected virtual void ConfigureContainer(Container container)
        {
            container.Register(made: Made.Of(() => new Server()));
            container.RegisterInitializer<Server>((server, c) =>
            {
                foreach (var service in c.ResolveMany<ServerServiceDefinition>())
                {
                    server.Services.Add(service);
                }
                foreach (var port in c.ResolveMany<ServerPort>())
                {
                    server.Ports.Add(port);
                }
            });

            container.RegisterDelegate((c) =>
            {
                var nodeContext = c.Resolve<Models.INodeContext>();
                var myNode = nodeContext.MyNode;
                var host = myNode.Host;
                var port = myNode.Port;
                return new ServerPort(host, port, ServerCredentials.Insecure);
            });
        }

        protected virtual CommandLineApplication CreateCommandLineApplication()
        {
            return new CommandLineApplication<TModel>();
        }

        protected virtual void ConfigureCommandLineApplication(CommandLineApplication app)
        {
            app.Conventions
               .UseOptionAttributes()
               .UseArgumentAttributes()
               .SetAppNameFromEntryAssembly();
            app.Description = Constants.CommandLineApplication.Description;
            app.HelpOption("-h|--help");
        }

        protected virtual TModel GetModel(CommandLineApplication app)
        {
            return (app as IModelAccessor).GetModel() as TModel;
        }

        protected virtual void OnInitActions(Container container)
        {
            foreach (var action in InitActions)
            {
                action.Invoke(container);
            }
        }

        public void AddInitAction(Action<Container> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException();
            }

            InitActions.Add(action);
        }

        protected IList<Action<Container>> InitActions { get; } = new List<Action<Container>>();

        private Logger Log { get; } = LogManager.GetCurrentClassLogger();
    }
}