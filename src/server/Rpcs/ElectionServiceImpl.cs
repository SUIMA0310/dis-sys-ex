using DryIoc;

using Grpc.Core;

using Rpc.Election;
using Rpc.Election.Utils;

using server.Expansions;
using server.Models;
using server.Services;

using System;
using System.Threading.Tasks;

namespace server.Rpcs
{
    /// <summary>
    /// Electionに関する要求を処理する
    /// </summary>
    public class ElectionServiceImpl : ElectionService.ElectionServiceBase
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// ApiServiceImpl Register in Container
        /// </summary>
        public static readonly Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<ElectionServiceImpl>();
            container.Register(made: Made.Of(() => ElectionService.BindService(Arg.Of<ElectionServiceImpl>())));
        };

        private readonly IOverlayNetwork overlayNetwork;
        private readonly INodeContext nodeContext;

        public ElectionServiceImpl(IOverlayNetwork _overlayNetwork, INodeContext _nodeContext)
        {
            overlayNetwork = _overlayNetwork;
            nodeContext = _nodeContext;
            Log.Trace("Constructed");
        }

        public override Task<ElectionResponse> Election(ElectionRequest request, ServerCallContext context)
        {
            try
            {
                Log.Info($"Receive election message from {request.Id}.");
                /* 最小のIDを持つNodeを選出する */
                if (nodeContext.MyNode.Id < request.Id)
                {
                    // 選任を引継ぎ継続する
                    overlayNetwork.Election();
                    // 選任を引き継いだことを通知
                    Log.Info($"Reply Ok.");
                    return ElectionResponseUtil.Ok.ToTask();
                }
                else
                {
                    Log.Info($"Reply No.");
                    // 引継ぎ要件を満たさない為、エラー
                    return ElectionResponseUtil.Error.ToTask();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Status = GetInternalStatus(ex);
                return ElectionResponseUtil.Error.ToTask();
            }
        }

        public override async Task<CoordinatorResponse> Coordinator(CoordinatorRequest request, ServerCallContext context)
        {
            try
            {
                Log.Info($"Receive Coordinator message from {request.Id},{request.Host}:{request.Port}.");
                if(request.Id < nodeContext.MyNode.Id)
                {
                    // Masterが自分より上位
                    await overlayNetwork.SetMasterAsync(request.ToNode());
                    return CoordinatorResponseUtil.Ok;
                }
                else
                {
                    // Masterが自分より下位の為、再選任
                    overlayNetwork.Election();
                    return CoordinatorResponseUtil.Error;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Status = GetInternalStatus(ex);
                return CoordinatorResponseUtil.Error;
            }
        }

        /// <summary>
        /// StatisCode.Internal の Statusを生成する
        /// </summary>
        private Status GetInternalStatus(Exception ex)
        {
            return new Status(StatusCode.Internal, ex.Message);
        }
    }
}