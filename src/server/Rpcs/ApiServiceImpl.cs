﻿using DryIoc;

using Grpc.Core;

using Rpc.Api;
using Rpc.Api.Utils;

using server.Expansions;
using server.Models;
using server.Services;

using System;
using System.Threading.Tasks;

namespace server.Rpcs
{
    /// <summary>
    /// ApiServiceの実装
    /// </summary>
    public class ApiServiceImpl : ApiService.ApiServiceBase
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// ApiServiceImpl Register in Container
        /// </summary>
        public static readonly Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<ApiServiceImpl>();
            container.Register(made: Made.Of(() => ApiService.BindService(Arg.Of<ApiServiceImpl>())));
        };

        /// <summary>
        /// ServiceEndPointのインスタンス
        /// </summary>
        private readonly IServiceEndPoint serviceEndPoint;

        /// <summary>
        /// ApiServiceImpl Constructor
        /// </summary>
        /// <param name="_serviceEndPoint"></param>
        public ApiServiceImpl(IServiceEndPoint _serviceEndPoint) : base()
        {
            serviceEndPoint = _serviceEndPoint;
            Log.Trace("Constructed");
        }

        /// <summary>
        /// ファイルの読み込みをサポートする
        /// </summary>
        public override async Task<ReadResponse> Read(ReadRequest request, ServerCallContext context)
        {
            Log.Info($"ReadRequest : {request.Name}");
            try
            {
                var file = await Task.Run(() => serviceEndPoint.ReadAsync(request.Name)).ConfigureAwait(false);
                if (file == default(File))
                {
                    Log.Info($"ReadRequest.NotFound");
                    context.Status = GetNotFoundStatus(request.Name);
                    return ReadResponseUtil.NotFound;
                }
                Log.Info($"ReadRequest.Ok");
                return file.ToReadResponse();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Status = GetInternalStatus(ex);
                return ReadResponseUtil.Error;
            }
        }

        /// <summary>
        /// ファイルの書き込みをサポートする
        /// </summary>
        public override async Task<WriteResponse> Write(WriteRequest request, ServerCallContext context)
        {
            Log.Info($"WriteRequest : {request.Name}");
            try
            {
                await Task.Run(() => serviceEndPoint.WriteAsync(request.ToFile()));
                Log.Info($"WriteRequest.Ok");
                return WriteResponseUtil.Ok;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Status = GetInternalStatus(ex);
                return WriteResponseUtil.Error;
            }
        }

        /// <summary>
        /// StatisCode.NotFound の Statusを生成する
        /// </summary>
        private Status GetNotFoundStatus(string fileName)
        {
            return new Status(StatusCode.NotFound, $"{fileName} is not found.");
        }

        /// <summary>
        /// StatisCode.Internal の Statusを生成する
        /// </summary>
        private Status GetInternalStatus(Exception ex)
        {
            return new Status(StatusCode.Internal, ex.Message);
        }
    }
}