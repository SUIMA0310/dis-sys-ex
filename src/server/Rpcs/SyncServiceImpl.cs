using DryIoc;

using Grpc.Core;

using Rpc.Sync;

using server.Expansions;
using server.Models;
using server.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Rpcs
{
    /// <summary>
    /// SyncServiceの実装
    /// </summary>
    public class SyncServiceImpl : SyncService.SyncServiceBase
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static readonly Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<SyncServiceImpl>();
            container.Register(made: Made.Of(() => SyncService.BindService(Arg.Of<SyncServiceImpl>())));
        };

        private readonly IDataStore dataStore;
        private readonly FileContext fileContext;
        private readonly List<IServerStreamWriter<SyncResponse>> streamWriters;

        public SyncServiceImpl(IDataStore _dataStore, FileContext _fileContext) : base()
        {
            dataStore = _dataStore;
            fileContext = _fileContext;
            streamWriters = new List<IServerStreamWriter<SyncResponse>>();

            // 自身がMaster時に、Secondaryへのデータ配信を行う
            dataStore.OnWriteSync(res =>
            {
                lock(streamWriters)
                {
                    Log.Info($"OnWriteSync : {res.FileId}, {res.Name}");
                    var tasks = streamWriters.Select(sw => sw.WriteAsync(res)).ToArray();
                    Task.WaitAll(tasks);
                }
            });
        }

        public override Task Sync(
            SyncRequest request,
            IServerStreamWriter<SyncResponse> responseStream,
            ServerCallContext context)
        {
            return Task.Run(async () =>
            {
                Log.Info($"Sync Request : {context.Peer}");
                try
                {
                    // MaxFileId以降のデータを共有する
                    IEnumerable<File> files;
                    lock(fileContext)
                    {
                        files = fileContext.Files.After(request.MaxFileId).ToArray();
                    }
                    foreach (var file in files)
                    {
                        Log.Trace($"Sync Step1 send : {context.Peer}, {file.Name}");
                        await responseStream.WriteAsync(file.ToSyncResponse()).ConfigureAwait(false);
                    }

                    // データ同期用のStreamListに追加する
                    lock(streamWriters)
                    {
                        Log.Trace($"Sync Step2 Add List : {context.Peer}");
                        streamWriters.Add(responseStream);
                    }

                    // データ同期が終了されるまで待つ
                    Log.Trace($"Sync Step3 Wait for Sync exit : {context.Peer}");
                    var token = dataStore.SyncCancelToken;
                    await Task.Run(() => token.WaitHandle.WaitOne()).ConfigureAwait(false);

                    // StritemListから自身のStreamを削除する
                    lock(streamWriters)
                    {
                        Log.Trace($"Sync Step4 Remove List : {context.Peer}");
                        streamWriters.Remove(responseStream);
                    }

                    Log.Info($"Sync exit : {context.Peer}");
                }
                catch (Exception ex)
                {
                    Log.Warn($"Sync faital : {context.Peer}");
                    Log.Warn(ex);
                    context.Status = new Status(StatusCode.Internal, ex.ToString());
                }
            });
        }
    }
}