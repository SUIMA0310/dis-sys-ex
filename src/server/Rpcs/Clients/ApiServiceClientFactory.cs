using DryIoc;

using Grpc.Core;

using Rpc.Api;

using System;

namespace server.Rpcs.Clients
{
    public class ApiServiceClientFactory : IClientFactory<ApiService.ApiServiceClient>
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<IClientFactory<ApiService.ApiServiceClient>, ApiServiceClientFactory>();
        };

        public ApiService.ApiServiceClient GetClient(Channel channel)
        {
            Log.Trace("Generated ApiServiceClient.");
            return new ApiService.ApiServiceClient(channel);
        }
    }
}