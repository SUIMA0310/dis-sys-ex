﻿using DryIoc;

using Grpc.Core;

using Rpc.Sync;

using System;

namespace server.Rpcs.Clients
{
    public class SyncServiceClientFactory : IClientFactory<SyncService.SyncServiceClient>
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<IClientFactory<SyncService.SyncServiceClient>, SyncServiceClientFactory>();
        };

        public SyncService.SyncServiceClient GetClient(Channel channel)
        {
            Log.Trace("Generated ElectionServiceClient.");
            return new SyncService.SyncServiceClient(channel);
        }
    }
}
