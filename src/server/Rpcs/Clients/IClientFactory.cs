using Grpc.Core;

namespace server.Rpcs.Clients
{
    public interface IClientFactory<TClient> where TClient : ClientBase
    {
        TClient GetClient(Channel channel);
    }
}