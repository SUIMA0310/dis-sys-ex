﻿using DryIoc;

using Grpc.Core;

using Rpc.Election;

using System;

namespace server.Rpcs.Clients
{
    public class ElectionServiceClientFactory : IClientFactory<ElectionService.ElectionServiceClient>
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<IClientFactory<ElectionService.ElectionServiceClient>, ElectionServiceClientFactory>();
        };

        public ElectionService.ElectionServiceClient GetClient(Channel channel)
        {
            Log.Trace("Generated ElectionServiceClient.");
            return new ElectionService.ElectionServiceClient(channel);
        }
    }
}