﻿using McMaster.Extensions.CommandLineUtils;

using System.ComponentModel.DataAnnotations;

namespace server
{
    public class Options
    {
        //[Option(
        //    "-p|--port <port-no>",
        //    Constants.CommandLineApplication.Descriptions.Port,
        //    CommandOptionType.SingleValue)]
        //public int Port { get; } = Constants.Grpc.DefaultPort;

        //[Option(
        //    "--host <host>",
        //    "",
        //    CommandOptionType.SingleValue)]
        //public string Host { get; } = Constants.Grpc.DefaultHost;

        [Option(
            "-f|--data-file <filePath>",
            Constants.CommandLineApplication.Descriptions.DataFile,
            CommandOptionType.SingleValue)]
        [LegalFilePath()]
        public virtual string DataFile { get; } = Constants.FileContext.DefaultDataFile;

        [Option(
            "-i|--id <InstanceId>",
            "インスタンスIDを指定",
            CommandOptionType.SingleValue)]
        [Required]
        public virtual int InstanceId { get; } = 0;

        [Option(
            "--network-file <filePath>",
            "OverlayNetworkに参加するホストの一覧",
            CommandOptionType.SingleValue)]
        [LegalFilePath()]
        public virtual string NetworkFile { get; } = "./net.json";

        //[Option(
        //    "--master",
        //    "プロセス自身がMasterかどうか",
        //    CommandOptionType.NoValue)]
        //public bool IsMaster { get; } = false;
    }
}