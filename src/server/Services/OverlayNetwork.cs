﻿using DryIoc;

using Grpc.Core;

using Rpc.Election;

using server.Expansions;
using server.Models;
using server.Rpcs.Clients;
using server.Utils;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using static Rpc.Election.ElectionService;

namespace server.Services
{
    public interface IOverlayNetwork
    {
        bool IsMaster { get; }

        Channel MasterChannel { get; }

        void OnMasterChanged(Action action);

        void Election();

        Task SetMasterAsync(Node node);
    }

    /// <summary>
    /// OverlayNetworkを制御する
    /// </summary>
    public class OverlayNetwork : IOverlayNetwork, IDisposable
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<IOverlayNetwork, OverlayNetwork>(Reuse.Singleton);
        };

        private readonly Options options;
        private readonly INodeContext nodeContext;
        private readonly IClientFactory<ElectionServiceClient> clientFactory;
        private readonly CancellationToken cancellationToken;

        private readonly SemaphoreSlim SetMasterLock = new SemaphoreSlim(1, 1);

        private IList<Action> _MasterChangedActions;
        private Node masterNode;
        private BackgroundTask NotifyMasterBackgroundTask;
        private BackgroundTask ConnectionBackgroundTask;
        private BackgroundTask ElectionBackgroundTask;

        public OverlayNetwork(
            Options _options,
            INodeContext _nodeContext,
            IClientFactory<ElectionServiceClient> _clientFactory,
            CancellationToken _cancellationToken)
        {
            options = _options;
            nodeContext = _nodeContext;
            clientFactory = _clientFactory;
            cancellationToken = _cancellationToken;

            // 他ノードとのチャンネルを確立する
            foreach (var node in nodeContext.OtherNodes)
            {
                node.CreateChannel();
            }

            // 初期状態として仮に自身をMasterとする
            IsMaster = true;
            MasterChannel = null;

            ElectionBackgroundTask = new BackgroundTask(
            async token =>
            {
                Log.Debug("選任プロセスを開始");

                // 自分のIDより小さなノードに対し順に問い合わせ
                foreach (var node in nodeContext
                    .OtherNodes
                    .OrderByDescending(x => x.Id)
                    .SkipWhile(x => options.InstanceId < x.Id))
                {
                    if (token.IsCancellationRequested)
                    {
                        Log.Debug("選任がキャンセルされました");
                        return;
                    }

                    try
                    {
                        Log.Debug($"選任を実施 : {node}");
                        var client = clientFactory.GetClient(node.Channel);
                        await node.Channel.ConnectAsync(DateTime.UtcNow.AddSeconds(3.0));
                        var ret = await client.ElectionAsync(
                            options.InstanceId.ToElectionRequest(),
                            deadline: DateTime.UtcNow.AddSeconds(3.0),
                            cancellationToken: token);

                        if (ret.Status == ElectionResponse.Types.ResStatus.Ok)
                        {
                            Log.Debug($"選任が引き継がれました : {node}");
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Debug($"接続できませんでした : {node}");
                        Log.Trace(ex);
                    }
                }

                if (token.IsCancellationRequested)
                {
                    Log.Debug("選任がキャンセルされました");
                    return;
                }

                // 自身をMasterとして設定する
                Log.Info("自ノードをMasterとして設定します");
                await SetMasterAsync(nodeContext.MyNode).ConfigureAwait(false);
            }
            , cancellationToken);

            NotifyMasterBackgroundTask = new BackgroundTask(
            token =>
            {
                var t_cancel = Task.Run(() => token.WaitHandle.WaitOne());
                return Task.WhenAll(nodeContext.OtherNodes.Select(async node =>
                {
                    try
                    {
                        Log.Debug($"自身がMasterであることを通知します : {node}");

                        while (!token.IsCancellationRequested)
                        {
                            var client = clientFactory.GetClient(node.Channel);

                            var task = node.Channel.ConnectAsync();
                            await Task.WhenAny(task, t_cancel).ConfigureAwait(false);

                            if (token.IsCancellationRequested)
                            {
                                Log.Debug($"通知がキャンセルされました : {node}");
                                break;
                            }

                            var ret = await client.CoordinatorAsync(
                                nodeContext.MyNode.ToCoordinatorRequest(),
                                deadline: DateTime.UtcNow.AddSeconds(5.0),
                                cancellationToken: token);

                            if (ret.Status == CoordinatorResponse.Types.ResStatus.Error)
                            {
                                Log.Debug($"失敗応答がありました : {node}");
                                break;
                            }
                            else if (ret.Status == CoordinatorResponse.Types.ResStatus.Ok)
                            {
                                Log.Debug($"Masterの通知に成功しました : {node}");
                                break;
                            }

                        }
                    }
                    catch (RpcException ex)
                    {
                        if(ex.StatusCode == StatusCode.Cancelled)
                        {
                            Log.Debug($"通知がキャンセルされました : {node}");
                        }
                        else
                        {
                            Log.Debug($"Masterの通知に失敗しました : {node}");
                            Log.Debug(ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Debug($"Masterの通知に失敗しました : {node}");
                        Log.Debug(ex);
                    }
                }));
            }
            , cancellationToken);

            ConnectionBackgroundTask = new BackgroundTask(
            async token =>
            {
                var channel = MasterChannel;
                Log.Debug("Connection Monitoring start.");
                try
                {
                    var state = channel.State;
                    if (state == ChannelState.Shutdown)
                    {
                        Log.Debug($"Channel closed.");
                        return;
                    }

                    var t_cancel = Task.Run(() => token.WaitHandle.WaitOne());

                    while (!token.IsCancellationRequested)
                    {
                        var task = channel.WaitForStateChangedAsync(state);

                        await Task.WhenAny(task, t_cancel).ConfigureAwait(false);

                        if (token.IsCancellationRequested)
                        {
                            return;
                        }

                        var newState = channel.State;

                        Log.Trace($"Channel state changed. {state} => {newState}");
                        state = newState;

                        if (state == ChannelState.Shutdown)
                        {
                            Log.Debug($"Channel closed.");
                            break;
                        }

                        if (state == ChannelState.TransientFailure ||
                            state == ChannelState.Idle)
                        {
                            Log.Debug($"Channel connection failure.");
                            Election();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Warn(ex);
                    Election();
                }
                Log.Debug("Connection Monitoring exit.");
            }
            , cancellationToken);

            // 起動時の初回選任を実行
            Election();

            Log.Trace("Constructed");
        }

        /// <summary>
        /// プロセス自身がMasterに選任されているか?
        /// <see langword="true"/> = Master
        /// <see langword="false"/> = Secondary
        /// </summary>
        public bool IsMaster { get; private set; }

        /// <summary>
        /// Masterとの通信経路
        /// <see langword="null"/> = 自身がMaster
        /// </summary>
        public Channel MasterChannel { get; private set; }

        /// <summary>
        /// 選任を行う
        /// </summary>
        public void Election()
        {
            ElectionBackgroundTask.Start();
        }

        /// <summary>
        /// 新たなMasterを指定する
        /// </summary>
        /// <param name="node"></param>
        public async Task SetMasterAsync(Node node)
        {
            await SetMasterLock.WaitAsync();
            try
            {
                if (masterNode != null && masterNode.Id == node.Id)
                {
                    // 自身がMasterであることを再通知
                    if (IsMaster)
                    {
                        await NotifyMasterBackgroundTask.StartAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        await NotifyMasterBackgroundTask.StopAsync().ConfigureAwait(false);
                    }
                    return;
                }
                masterNode = node;

                Log.Debug($"Masterを変更 : {node}");
                var master = nodeContext.GetNodeFromId(node.Id);
                if (master.Host != node.Host || master.Port != node.Port)
                {
                    throw new Exception("remote host or port is not match.");
                }

                if (master != nodeContext.MyNode)
                {
                    IsMaster = false;
                    MasterChannel = master.Channel;

                    await NotifyMasterBackgroundTask.StopAsync().ConfigureAwait(false);
                    await ConnectionBackgroundTask.StartAsync().ConfigureAwait(false);
                }
                else
                {
                    IsMaster = true;
                    MasterChannel = null;

                    await NotifyMasterBackgroundTask.StartAsync().ConfigureAwait(false);
                }

                await MasterChangedInvokeAsync().ConfigureAwait(false);
            }
            finally
            {
                SetMasterLock.Release();
            }
        }

        /// <summary>
        /// Masterが変化したことを通知する
        /// </summary>
        public void OnMasterChanged(Action action)
        {
            Log.Trace("OnMasterChanged set action");
            action = action ?? throw new ArgumentNullException(nameof(action));

            _MasterChangedActions = _MasterChangedActions ?? new List<Action>();
            _MasterChangedActions.Add(action);
        }

        /// <summary>
        /// Masterの変化を通知する
        /// </summary>
        /// <returns></returns>
        private Task MasterChangedInvokeAsync()
        {
            if (_MasterChangedActions == null)
            {
                return Task.CompletedTask;
            }

            Log.Trace($"MasterChangedInvoke");
            return Task.WhenAll(
                _MasterChangedActions
                .Select(a => Task.Run(() => a.Invoke()))
            );
        }

        public void Dispose()
        {
            ElectionBackgroundTask.Dispose();
            ConnectionBackgroundTask.Dispose();
            NotifyMasterBackgroundTask.Dispose();

            SetMasterLock.Dispose();
        }
    }
}