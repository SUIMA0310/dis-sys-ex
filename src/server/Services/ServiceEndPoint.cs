﻿using DryIoc;

using Rpc.Api;

using server.Expansions;
using server.Models;
using server.Rpcs.Clients;

using System;
using System.Threading.Tasks;

namespace server.Services
{
    public interface IServiceEndPoint
    {
        Task<File> ReadAsync(string name);

        Task WriteAsync(File file);
    }

    public class ServiceEndPoint : IServiceEndPoint
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<IServiceEndPoint, ServiceEndPoint>(Reuse.Singleton);
        };

        private readonly IDataStore dataStore;
        private readonly IOverlayNetwork overlayNetwork;
        private readonly IClientFactory<ApiService.ApiServiceClient> clientFactory;

        public ServiceEndPoint(
            IDataStore _dataStore,
            IOverlayNetwork _overlayNetwork,
            IClientFactory<ApiService.ApiServiceClient> _clientFactory)
        {
            dataStore = _dataStore;
            overlayNetwork = _overlayNetwork;
            clientFactory = _clientFactory;
            Log.Trace("Constructed");
        }

        public Task<File> ReadAsync(string name)
        {
            Log.Info($"Read : {name}");
            return dataStore.ReadAsync(name);
        }

        public async Task WriteAsync(File file)
        {
            Log.Info($"Write : {file.Name}");
            if (overlayNetwork.IsMaster)
            {
                Log.Info($"Save to DataStore.");
                await dataStore.WriteAsync(file);
            }
            else
            {
                Log.Info($"Transfer to master.");
                var client = clientFactory.GetClient(overlayNetwork.MasterChannel);
                var ret = await client.WriteAsync(file.ToWriteRequest());
                if (ret.Status != WriteResponse.Types.ResStatus.Ok)
                {
                    Log.Error($"Transfer failure.");
                    throw new Exception($"Transfer failure.");
                }
            }
        }
    }
}