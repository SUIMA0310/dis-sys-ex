﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DryIoc;
using Grpc.Core;
using Rpc.Sync;
using server.Expansions;
using server.Models;
using server.Rpcs.Clients;
using server.Utils;

using static Rpc.Sync.SyncService;

namespace server.Services
{
    /// <summary>
    /// IDataStore
    /// </summary>
    public interface IDataStore
    {
        Task<File> ReadAsync(string name);

        Task WriteAsync(File file);

        void OnWriteSync(Action<SyncResponse> action);

        CancellationToken SyncCancelToken { get; }
    }

    /// <summary>
    /// 抽象化されたデータ格納クラス
    /// - Disposable
    /// </summary>
    public class DataStore : IDataStore, IDisposable
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<IDataStore, DataStore>(Reuse.Singleton);
        };

        private readonly FileContext fileContext;
        private readonly IOverlayNetwork overlayNetwork;
        private readonly IClientFactory<SyncServiceClient> clientFactory;
        private IList<Action<SyncResponse>> _WriteSyncActions;
        private BackgroundTask SyncBackgroundTask;

        private CancellationTokenSource SyncCancelTokenSource;
        public CancellationToken SyncCancelToken 
        {
            get
            {
                lock(SyncCancelTokenSource)
                {
                    return SyncCancelTokenSource.Token;
                }
            }
        }

        /// <summary>
        /// Constructor
        /// - DIコンテナにより、インスタンス化
        /// </summary>
        /// <param name="_fileContext"></param>
        /// <param name="_overlayNetwork"></param>
        public DataStore(
            FileContext _fileContext,
            IOverlayNetwork _overlayNetwork,
            IClientFactory<SyncServiceClient> _clientFactory,
            CancellationToken cancellationToken)
        {
            fileContext = _fileContext;
            overlayNetwork = _overlayNetwork;
            clientFactory = _clientFactory;

            SyncCancelTokenSource = GetCancelTokenSource(cancellationToken);

            SyncBackgroundTask = new BackgroundTask(
                async token =>
                {
                    if (overlayNetwork.IsMaster)
                    {
                        // Masterの場合、同期は行わない
                        return;
                    }

                    Log.Info($"Sync Client start.");
                    while (true)
                    {
                        try
                        {
                            token.ThrowIfCancellationRequested();
                            var maxFileId = fileContext.Files.MaxFileId();

                            var masterChannel = overlayNetwork.MasterChannel;
                            if (overlayNetwork.IsMaster)
                            {
                                Log.Info($"Sync Client cancel.");
                                return;
                            }

                            var serviceClient = clientFactory.GetClient(masterChannel);
                            var req = new SyncRequest { MaxFileId = maxFileId };
                            using (var res = serviceClient.Sync(req, cancellationToken: token))
                            using (var stream = res.ResponseStream)
                            {
                                token.ThrowIfCancellationRequested();
                                while (await stream.MoveNext(token))
                                {
                                    var file = stream.Current.ToFile();
                                    Log.Info($"Sync receive : {file.FileId},{file.Name}");
                                    fileContext.Files.Add(file);
                                    await fileContext.SaveChangesAsync();
                                }
                            }
                        }
                        catch (RpcException ex)
                        {
                            Log.Trace($"Sync Client retry. {ex.ToString()}");
                        }
                        catch (OperationCanceledException)
                        {
                            break;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                            break;
                        }
                    }
                    Log.Info($"Sync Client exit.");
                }, cancellationToken);

            overlayNetwork.OnMasterChanged(() =>
            {
                Log.Debug("Master changed.");
              
                lock(SyncCancelTokenSource)
                {
                    SyncCancelTokenSource.Cancel();
                    SyncCancelTokenSource.Dispose();
                    SyncCancelTokenSource = GetCancelTokenSource(cancellationToken);
                }

                // Masterとの同期を開始
                SyncBackgroundTask.Start();
            });

            // Masterとの同期を開始
            SyncBackgroundTask.Start();

            Log.Trace("Constructed");
        }

        /// <summary>
        /// 抽象化されたデータ格納クラスから、データを取り出す
        /// </summary>
        public async Task<File> ReadAsync(string name)
        {
            Log.Info($"Read : {name}");
            var file = await Task.Run(() => fileContext.Files.Search(name));
            return file;
        }

        /// <summary>
        /// 抽象化されたデータ格納クラスへ、データを書き込む
        /// </summary>
        /// <param name="file"></param>
        public async Task WriteAsync(File file)
        {
            Log.Info($"Write : {file.Name}");
            Task saveTask, syncTask = Task.CompletedTask;
            fileContext.Files.Add(file);
            saveTask = fileContext.SaveChangesAsync();

            // SaveChangesと平行して行う
            if (overlayNetwork.IsMaster)
            {
                syncTask = WriteSyncInvokeAsync(file.ToSyncResponse());
            }

            await Task.WhenAll(saveTask, syncTask);
        }

        /// <summary>
        /// MasterからSecondaryへの同期を通知する
        /// </summary>
        /// <param name="action">同期を行う処理</param>
        public void OnWriteSync(Action<SyncResponse> action)
        {
            Log.Trace("OnWriteSync set action");
            action = action ??
                throw new ArgumentNullException(nameof(action));

            _WriteSyncActions = _WriteSyncActions ?? new List<Action<SyncResponse>>();
            _WriteSyncActions.Add(action);
        }

        /// <summary>
        /// MasterからSecondaryへのデータ同期を行う
        /// </summary>
        /// <param name="response"></param>
        private Task WriteSyncInvokeAsync(SyncResponse response)
        {
            if (_WriteSyncActions == null)
            {
                return Task.CompletedTask;
            }

            Log.Trace($"WriteSyncInvoke");
            return Task.WhenAll(
                _WriteSyncActions
                .Select(a => Task.Run(() => 
                {
                    try
                    {
                        a.Invoke(response);
                    }
                    catch(Exception ex)
                    {
                        Log.Warn($"Write Sync Error.");
                        Log.Trace(ex);
                    }
                }))
            );
        }

        private CancellationTokenSource GetCancelTokenSource(CancellationToken token)
        {
            Log.Trace($"Create new CancelTokenSource");
            return CancellationTokenSource.CreateLinkedTokenSource(token);
        }

        /// <summary>
        /// 参照を捨てて、インスタンス破棄の用意をする
        /// </summary>
        public void Dispose()
        {
            Log.Trace($"Dispose now");

            _WriteSyncActions.Clear();
            _WriteSyncActions = null;

            SyncBackgroundTask.Dispose();
        }
    }
}