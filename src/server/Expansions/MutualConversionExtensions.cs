using Rpc.Api;
using Rpc.Election;
using Rpc.Sync;

using server.Models;

using System;
using System.Threading.Tasks;

namespace server.Expansions
{
    public static class WriteRequestExtensions
    {
        /// <summary>
        /// server.Models.File に変換する
        /// </summary>
        public static File ToFile(this WriteRequest request)
        {
            return new File
            {
                Name = request.Name,
                Content = request.Content.ToByteArray(),
                InsertTime = DateTime.Now
            };
        }
    }

    public static class FileExtensions
    {
        /// <summary>
        /// Rpc.Api.ReadResponse に変換する
        /// </summary>
        public static ReadResponse ToReadResponse(
            this File file,
            ReadResponse.Types.ResStatus status = ReadResponse.Types.ResStatus.Ok)
        {
            return new ReadResponse()
            {
                Status = status,
                Content = file.Content.ToByteString()
            };
        }

        public static SyncResponse ToSyncResponse(this File file)
        {
            return new SyncResponse()
            {
                FileId = file.FileId,
                Name = file.Name,
                Content = file.Content.ToByteString()
            };
        }

        public static WriteRequest ToWriteRequest(this File file)
        {
            return new WriteRequest()
            {
                Name = file.Name,
                Content = file.Content.ToByteString()
            };
        }
    }

    public static class SyncResponseExtensions
    {
        /// <summary>
        /// server.Models.File に変換する
        /// </summary>
        public static File ToFile(this SyncResponse response)
        {
            return new File()
            {
                FileId = response.FileId,
                Name = response.Name,
                Content = response.Content.ToByteArray(),
                InsertTime = DateTime.Now
            };
        }
    }

    public static class CoordinatorRequestExtensions
    {
        /// <summary>
        /// server.Models.Node に変換する
        /// </summary>
        public static Node ToNode(this CoordinatorRequest req)
        {
            return new Node()
            {
                Id = req.Id,
                Host = req.Host,
                Port = req.Port
            };
        }
    }

    public static class Int32Extensions
    {
        /// <summary>
        /// MaxFileIdから、SyncRequestに変換する
        /// </summary>
        /// <param name="maxFileId"></param>
        /// <returns></returns>
        public static SyncRequest ToSyncRequest(this int maxFileId)
        {
            return new SyncRequest()
            {
                MaxFileId = maxFileId
            };
        }

        /// <summary>
        /// サーバノードのインスタンスIDからElectionRequestを作成する
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ElectionRequest ToElectionRequest(this int id)
        {
            return new ElectionRequest()
            {
                Id = id
            };
        }
    }

    public static class NodeExtensions
    {
        public static CoordinatorRequest ToCoordinatorRequest(this Node myNode)
        {
            return new CoordinatorRequest()
            {
                Id = myNode.Id,
                Host = myNode.Host,
                Port = myNode.Port
            };
        }
    }

    public static class TaskExtensions
    {
        public static Task<T> ToTask<T>(this T from) where T : class
        {
            return Task.FromResult(from);
        }
    }

    public static class ByteArrayExtensions
    {
        public static Google.Protobuf.ByteString ToByteString(this byte[] bytes)
        {
            return Google.Protobuf.ByteString.CopyFrom(bytes);
        }
    }

    public static class JsonExtensions
    {
        public static string ToJson<T>(this T obj) where T : class
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);
        }

        public static T ToObject<T>(this string json) where T : class
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
    }
}