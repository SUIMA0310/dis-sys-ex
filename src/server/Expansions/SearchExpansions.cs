﻿using server.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace server.Expansions
{
    public static class SearchExpansions
    {
        public static File Search(this IQueryable<File> files, string name)
        {
            return files
                .Where(e => e.Name == name)
                .OrderBy(e => e.FileId)
                .FirstOrDefault();
        }

        public static IEnumerable<File> After(this IQueryable<File> files, int fileId)
        {
            return files
                .Where(e => e.FileId > fileId)
                .OrderBy(e => e.FileId);
        }

        public static bool HasDuplicate<TSource, TKey>(
            this IEnumerable<TSource> list,
            Func<TSource, TKey> keySelector)
        {
            return list.GroupBy(keySelector).Any(x => x.Skip(1).Any());
        }

        public static int MaxFileId(this IQueryable<File> files)
        {
            try
            {
                return files.Max(e => e.FileId);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}