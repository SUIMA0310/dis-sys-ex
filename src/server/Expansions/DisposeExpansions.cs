﻿using System;
using System.Collections.Generic;

namespace server.Expansions
{
    public static class DisposeExpansions
    {
        public static void Disposes(this IEnumerable<IDisposable> list)
        {
            foreach (var item in list)
            {
                item.Dispose();
            }
        }
    }
}