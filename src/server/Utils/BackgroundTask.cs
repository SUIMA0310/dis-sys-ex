﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace server.Utils
{
    public class BackgroundTask : IDisposable
    {
        private readonly CancellationToken ParentToken;
        private readonly Func<CancellationToken, Task> InvokeFunc;

        private readonly SemaphoreSlim StartLock = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim StopLock = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim FieldLock = new SemaphoreSlim(1, 1);

        private Task InnerTask;
        private CancellationTokenSource CancellationTokenSource;

        public BackgroundTask(Func<CancellationToken, Task> func)
        {
            InvokeFunc = func ?? throw new ArgumentNullException(nameof(func));
            ParentToken = CancellationToken.None;
        }

        public BackgroundTask(Func<CancellationToken, Task> func, CancellationToken token)
        {
            InvokeFunc = func ?? throw new ArgumentNullException(nameof(func));
            ParentToken = token;
        }

        public void Dispose()
        {
            Stop();

            StartLock.Dispose();
            StopLock.Dispose();
            FieldLock.Dispose();
        }

        public async Task StartAsync()
        {
            await StartLock.WaitAsync().ConfigureAwait(false);
            try
            {
                // 実行中の処理を停止
                await StopAsync().ConfigureAwait(false);

                await FieldLock.WaitAsync().ConfigureAwait(false);
                try
                {
                    // 新しいキャンセラを用意
                    CancellationTokenSource = GetCancellationTokenSource();

                    // 新規のタスクを用意
                    InnerTask = GetInnerTask(CancellationTokenSource.Token);

                    // タスクを実行開始
                    InnerTask.Start();
                }
                finally
                {
                    FieldLock.Release();
                }
            }
            finally
            {
                StartLock.Release();
            }
        }

        public async Task StopAsync()
        {
            await StopLock.WaitAsync().ConfigureAwait(false);
            try
            {
                await FieldLock.WaitAsync().ConfigureAwait(false);
                try
                {

                    if (InnerTask != null)
                    {
                        // Taskを終わらせる
                        CancellationTokenSource.Cancel();

                        // Taskの完了を待機
                        try
                        {
                            await InnerTask.ConfigureAwait(false);
                        }
                        catch (TaskCanceledException)
                        {
                            // タスクがキャンセルされていても何もしない
                        }

                        // Taskの破棄処理
                        InnerTask.Dispose();
                        InnerTask = null;
                    }

                    // キャンセラの破棄処理
                    CancellationTokenSource?.Dispose();
                    CancellationTokenSource = null;
                }
                finally
                {
                    FieldLock.Release();
                }
            }
            finally
            {
                StopLock.Release();
            }
        }

        public void Start()
        {
            StartAsync().Wait();
        }

        public void Stop()
        {
            StopAsync().Wait();
        }

        private Task GetInnerTask(CancellationToken token)
        {
            return new Task(async () => await InvokeFunc(token), token);
        }

        private CancellationTokenSource GetCancellationTokenSource()
        {
            return CancellationTokenSource.CreateLinkedTokenSource(ParentToken);
        }

    }
}
