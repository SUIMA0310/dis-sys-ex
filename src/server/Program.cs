﻿namespace server
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var bootstrap = new Bootstrap<Options>();
            bootstrap.AddInitAction(Models.FileContext.InitAction);
            bootstrap.AddInitAction(Models.NodeContext.InitAction);

            bootstrap.AddInitAction(Rpcs.ApiServiceImpl.InitAction);
            bootstrap.AddInitAction(Rpcs.SyncServiceImpl.InitAction);
            bootstrap.AddInitAction(Rpcs.ElectionServiceImpl.InitAction);

            bootstrap.AddInitAction(Rpcs.Clients.ApiServiceClientFactory.InitAction);
            bootstrap.AddInitAction(Rpcs.Clients.SyncServiceClientFactory.InitAction);
            bootstrap.AddInitAction(Rpcs.Clients.ElectionServiceClientFactory.InitAction);

            bootstrap.AddInitAction(Services.DataStore.InitAction);
            bootstrap.AddInitAction(Services.ServiceEndPoint.InitAction);
            bootstrap.AddInitAction(Services.OverlayNetwork.InitAction);
            return bootstrap.Run(args);
        }
    }
}