using DryIoc;

using Newtonsoft.Json;

using server.Expansions;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace server.Models
{
    public interface INodeContext
    {
        IEnumerable<Node> AllNodes { get; }
        IEnumerable<Node> OtherNodes { get; }

        Node GetNodeFromId(int id);

        Node MyNode { get; }
    }

    public class NodeContext : INodeContext, IDisposable
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public static Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.Register<INodeContext, NodeContext>(Reuse.Singleton);
        };

        private Options options;

        public NodeContext(Options _options)
        {
            options = _options;
            using (var sr = new StreamReader(options.NetworkFile))
            {
                AllNodes = JsonConvert.DeserializeObject<IEnumerable<Node>>(sr.ReadToEnd());
            }

            if (AllNodes.HasDuplicate(x => x.Id))
            {
                throw new Exception($"Network File Error : IDが重複しています");
            }

            if (AllNodes.HasDuplicate(x => $"{x.Host}:{x.Port}"))
            {
                throw new Exception($"Network File Error : EndPointが重複しています");
            }
        }

        public IEnumerable<Node> AllNodes { get; }

        public Node MyNode => GetNodeFromId(options.InstanceId);

        public IEnumerable<Node> OtherNodes => AllNodes.Where(x => x != MyNode);

        public Node GetNodeFromId(int id)
        {
            return AllNodes.FirstOrDefault(x => x.Id == id);
        }

        public void Dispose()
        {
            AllNodes.Disposes();
        }
    }
}