﻿using Grpc.Core;

using Newtonsoft.Json;

using System;
using System.Threading.Tasks;

namespace server.Models
{
    [JsonObject]
    public class Node : IDisposable
    {
        [JsonProperty]
        public virtual string Host { get; set; }

        [JsonProperty]
        public virtual int Port { get; set; }

        [JsonProperty]
        public virtual int Id { get; set; }

        [JsonIgnore]
        public virtual Channel Channel => _Channel ?? CreateChannel();
        
        /// <summary>
        /// プロパティのChannelを作成する
        /// </summary>
        /// <returns></returns>
        public virtual Channel CreateChannel()
        {
            if (_Channel != null)
            {
                return _Channel;
            }

            _Channel = new Channel(Host, Port, ChannelCredentials.Insecure);
            return _Channel;
        }

        public virtual void Dispose()
        {
            _Channel?.ShutdownAsync().Wait();
        }

        public override string ToString()
        {
            return $"[{Id}]{Host}:{Port}";
        }

        private Channel _Channel = null;
    }
}