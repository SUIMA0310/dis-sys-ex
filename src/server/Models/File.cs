using System;
using System.ComponentModel.DataAnnotations;

namespace server.Models
{
    public class File
    {
        [Key]
        public virtual int FileId { get; set; }

        [Required]
        public virtual string Name { get; set; }

        [Required]
        public virtual byte[] Content { get; set; }

        [Required]
        public virtual DateTime InsertTime { get; set; }
    }
}