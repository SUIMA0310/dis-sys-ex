using DryIoc;

using Microsoft.EntityFrameworkCore;

using System;

namespace server.Models
{
    public class FileContext : DbContext
    {
        public static readonly Action<Container> InitAction = container =>
        {
            Log.Trace("Register in Container...");
            container.RegisterDelegate(c =>
            {
                var options = c.Resolve<Options>();
                var dbContextOptions = new DbContextOptionsBuilder<FileContext>()
                    .UseSqlite($"Data Source={options.DataFile}")
                    .Options;
                return new FileContext(dbContextOptions);
            }, Reuse.Singleton);
        };

        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public FileContext(DbContextOptions<FileContext> options) : base(options)
        {
            Database.EnsureCreated();
            Log.Trace("Constructed");
        }

        public virtual DbSet<File> Files { get; set; }
    }
}