setlocal

@rem このディレクトリがカレントディレクトリとなる様に設定する
cd /d %~dp0

@rem nugetのキャッシュからexeを持ってくるので、先に "dotnet restore" を実行しておいてください。
set PROTOC=%UserProfile%\.nuget\packages\grpc.tools\1.16.0\tools\windows_x64\protoc.exe
set PLUGIN=%UserProfile%\.nuget\packages\grpc.tools\1.16.0\tools\windows_x64\grpc_csharp_plugin.exe

set SRC_DIR=../src/protos
set DST_DIR=../src/rpcs/api
set PROTO=api.proto

%PROTOC% -I%SRC_DIR% --csharp_out %DST_DIR%  %SRC_DIR%/%PROTO% --grpc_out %DST_DIR% --plugin=protoc-gen-grpc=%PLUGIN%

endlocal
