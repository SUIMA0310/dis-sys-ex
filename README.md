﻿# 分散システム実践論 演習課題
分散システム実践論 演習課題のリポジトリです.  
試行錯誤しながら進めていっているため、ブランチは迷走しています.  
一応、“Master”は安定させているつもり().

## 要求仕様
1. Client - Server モデル
2. Clientは、以下のAPIを実装
    1. read
    2. write
3. Serverのインスタンスは複数設ける
    1. 1つのMasterと複数のSecondaryを持つ
    2. 更新動作は、Masterで処理が行われたのち、Secondaryに伝播する
    3. 読み取り動作は、任意のインスタンス(Master or Secondary)で行う
4. RPC 及び DNS を利用する
5. Masterに障害が発生した際には、選任アルゴリズムで新たなMasterを選出する
6. 耐故障性を提供する

# 動作環境
[.NET Core][dotnet] 2.1.500
Network接続

# 実行手順
予め、Gitをインストールしておいてください.  
.NET Core 2.1 SDKがインストール済みであれば、次の手順へ
## 1, [.NET Core 2.1 SDK][dotnet-sdk]のインストール
### ダウンロード
- Windows
    - [x64 Installer](https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.403-windows-x64-installer)
    | [x86 Installer](https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.403-windows-x86-installer)
- Max
    - [Installer](https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.403-macos-x64-installer)
- Linux
    - Ubuntu 18.04の場合  
    ```
    $ > wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
    $ > sudo dpkg -i packages-microsoft-prod.deb
    $ > sudo add-apt-repository universe
    $ > sudo apt-get install apt-transport-https
    $ > sudo apt-get update
    $ > sudo apt-get install dotnet-sdk-2.1
    ```

### インストールの確認
`dotnet --version`
```
$ > dotnet --version
2.1.500
```

"注釈" : 実行時にBuild操作が必要なため、ランタイムではなくSDKをインストールしてください。
## 2, Repositoryをcloneする
`https://gitlab.com/SUIMA0310/dis-sys-ex.git`
```
$ > git clone https://gitlab.com/SUIMA0310/dis-sys-ex.git
```

## 3, Buildする
`cd dis-sys-ex`で,ディレクトリに入る.
```
$ > cd dis-sys-ex
$ dis-sys-ex> dotnet build
```
初回のBuildでは,依存パッケージのダウンロードも同時に行うため,時間がかかります.

# 使用方法
## Client
```
dotnet client.dll [Arg:host] [Options] [Command] ...
```

| 引数 | 意味                 | 使用例                                                            |
| ---- | -------------------- | ----------------------------------------------------------------- |
| host | 接続ホストを指定する | `dotnet client.dll prac1.ds.soft.iwate-pu.ac.jp write ./hoge.txt` |

| オプション             | 意味                 | 使用例                                                  |
| ---------------------- | -------------------- | ------------------------------------------------------- |
| -h&#124;--help         | ヘルプを表示する     | `dotnet client.dll --help`                              |
| -p&#124;--port \<port> | 接続ポートを指定する | `dotnet client.dll localhost -p 50024 write ./hoge.txt` |

| コマンド | 意味                     | 使用例                                             |
| -------- | ------------------------ | -------------------------------------------------- |
| write    | サーバへの書き込みを行う | `dotnet client.dll localhost write ./test.txt`     |
| read     | サーバから読み取りを行う | `dotnet client.dll localhost read ./read/test.txt` |

## Client.Write
| 引数  | 意味                                   | 使用例                                                    |
| ----- | -------------------------------------- | --------------------------------------------------------- |
| files | 書き込むファイルを指定する(複数選択可) | `dotnet client.dll localhost write ./hoge.txt ./fuga.txt` |

| オプション     | 意味             | 使用例                           |
| -------------- | ---------------- | -------------------------------- |
| -h&#124;--help | ヘルプを表示する | `dotnet client.dll write --help` |

## Client.Read
| 引数  | 意味                                   | 使用例                                                             |
| ----- | -------------------------------------- | ------------------------------------------------------------------ |
| files | 読み込むファイルを指定する(複数選択可) | `dotnet client.dll localhost read ./read/hoge.txt ./read/fuga.txt` |

| オプション     | 意味             | 使用例                          |
| -------------- | ---------------- | ------------------------------- |
| -h&#124;--help | ヘルプを表示する | `dotnet client.dll read --help` |

## Server
```
dotnet server.dll [Options]
```

### Options
| オプション                      | 意味                                                   | 使用例                                        |
| ------------------------------- | ------------------------------------------------------ | --------------------------------------------- |
| -h&#124;--help                  | ヘルプを表示                                           | `dotnet server.dll --help`                    |
| -i&#124;--id \<instanceId>      | OverlayNetwork上でのIDを指定                           | `dotnet server.dll --id 1`                    |
| -f&#124;--data-file \<filePath> | データベースファイルを指定                             | `dotnet server.dll --data-file ./server.db`   |
| --network-file \<filePath>      | OverlayNetworkに参加する<br>ホスト一覧のファイルを指定 | `dotnet server.dll --network-file ./net.json` |

[dotnet]:https://www.microsoft.com/net
[dotnet-sdk]:https://www.microsoft.com/net/learn/dotnet/hello-world-tutorial